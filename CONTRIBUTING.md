# Contributing to Morse-Game-Pi-Pico
Hello and welcome! We are so glad that you are interested in contributing to the Morse-Game-Pi-Pico Project!
We only have a couple of rules and we hope you enjoy the process :)

## Contributing Rules
1. Don't move or delete any files. Only modify them.
2. Put all ARM (assembly) code in the file **assign02.s**.
3. Put all C code in the file **assign02.c**.

## Contributing Process
1. Head to the issues tab and look for an issue that you like unless one has been assigned to you.
2. Once you have decided what issue to work on, give it a shot! 
3. Clone this repository to your computer.
4. Before you make any changes, switch to the branch named after you.
5. Work on the issue. Once done, push the code to your branch within this repository.
6. Head to the Merge Request notification that pops up on your branch / **compare** section under the repository tab and create a Merge Request to the main branch (Remember to compare only **Your** branch alongside the main branch. Add a small description to the Merge Request describing what you've done : Mention what issue you have worked on. If the issue number is #3, you can mention "Closes #3" in the Merge Request description)
7. Wait for your work to be tested and merged with the main branch !

It's that easy! We hope you enjoy contributing to our repository. Don't hesitate to contact any of the maintainers about any problems!
