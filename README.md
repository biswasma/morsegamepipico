# Morse-Game-Pi-Pico

A repository containing code for a game that teaches the basics of Morse Code with the help of a Raspberry Pi Pico to all its players - built for Microprocessor Systems Group Assignment (2).

# Overview:
A game that teaches the player the fundamentals of Morse code has been developed. This game has been written in a mixture of both C code and ARM assembly language. The player interacts with this game by pressing the GP21 button on the MAKER-PI-PICO board for a short duration to input a Morse “dot” and a longer duration to input a Morse “dash”. If no new input is entered for at least 1 second after one-or-more “dots” or “dashes” have been input, then it is considered to be a “space” character. If no new input is entered for at least another 1 second after that, then the sequence is considered complete and passed to the game portion of the code for pattern matching.

# Basic gameplay specifications:
- As soon as the game is started, the UART console displays a welcome message banner that includes the group number as well as some brief instructions on how to play the game. The RGB LED is set to the colour *blue* to indicate that a game is not in progress.
- The player is presented with a choice of **four** difficulty levels (chosen by entering the equivalent Morse code pattern for the level number) to start the game.
- When the game starts, the player is presented with an alphanumeric character (A-Z/0-9). The player should then attempt to enter the equivalent Morse code sequence for the character using the GP21 button. If the player enters the correct Morse code sequence, then the answer is correct, otherwise it is incorrect. The sequence that the player inputs is shown as both a Morse code pattern as well as the alphanumeric character equivalent that it decodes to (if it does decode to a character). If it does not decode to an alphanumeric character, then a “?” is shown instead.
- The four levels of the game work as follows:
    - Level #1: Individual characters with their equivalent Morse code provided.
    - Level #2: Individual characters without their equivalent Morse code provided.
    - Level #3: Individual words with their equivalent Morse code provided.
    - Level #4: Individual words without their equivalent Morse code provided.
- At the commencement of a level, the player starts with three lives, denoted by the RGB LED flashing *green*. Each time the player inputs a sequence that is not accurate to the expected sequence that is provided, a life is taken away (maximum of *3*) and the RGB LED colour flashes accordingly.
- If the number of lives reaches zero, then the game is over and the RGB LED flashes *red* to indicate the same.
- To progress to the next level, the player should enter 5 fully correct sequences in a row during the current level. If the player is already at the final level, then they have completed the game !

# Team Members and Task Breakdown
- **Prabhpreet Singh** - *Project Documentation Owner*
- **Manasvi Prakash** - *Project Demonstration Owner*
- **Ujjayant Kadian** - *Project Code Owner*
- **Yatharth jain** - *Project Workflow Owner*
- **Manab Kumar Biswas** - *GitLab Workflow Owner*

# Setup Proccedure
This repository has been designed and built with a smooth setup proceedure in mind. We would require you to:

1. Clone this repository to your computer.
2. Use VS Code to open this project.
3. Ensure you have all the extensions and pre-requisites set up as per the *pico-apps* repository.
4. Navigate to the file *.vscode/settings.json* and replace the path for the PICO SDK (line 23) with the path on your machine.
5. Set the build target to *all* and run the build.
6. Once a successfull run is complete, set the target to *assign02* and run the build yet again. 

And you have successfully compiled and executed the code on your machine !

# Contributing to Morse-Game-Pi-Pico
Hello and welcome! We are so glad that you are interested in contributing to the Morse-Game-Pi-Pico Project!
We only have a couple of rules and we hope you enjoy the process :)

## Contributing Rules
1. Don't move or delete any files. Only modify them.
2. Put all ARM (assembly) code in the file **assign02.s**.
3. Put all C code in the file **assign02.c**.

## Contributing Process
1. Head to the issues tab and look for an issue that you like unless one has been assigned to you.
2. Once you have decided what issue to work on, give it a shot! 
3. Clone this repository to your computer.
4. Before you make any changes, switch to the branch named after you.
5. Work on the issue. Once done, push the code to your branch within this repository.
6. Head to the Merge Request notification that pops up on your branch / **compare** section under the repository tab and create a Merge Request to the main branch (Remember to compare only **Your** branch alongside the main branch. Add a small description to the Merge Request describing what you've done : Mention what issue you have worked on. If the issue number is #3, you can mention "Closes #3" in the Merge Request description)
7. Wait for your work to be tested and merged with the main branch !

It's that easy! We hope you enjoy contributing to our repository. Don't hesitate to contact any of the maintainers about any problems!
